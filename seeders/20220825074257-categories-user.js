'use strict';

module.exports = {
  async up (queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Categories', [{
      name: 'men formal wear',
      description:'formal clothing includes trousers, some form of neckwear suit accessory and dress shoes.',
      GroupId:1,
      isactive:'true',
      createdAt: new Date(),
      updatedAt: new Date(),
      },{
      name: 'men ethnic wear',
      description:'ethnic clothing includes casual, smart casual,business formal, cocktail attire, semi formal,',
      GroupId:1,
      isactive:'true',
      createdAt: new Date(),
      updatedAt: new Date(),
      },{
      name: 'women formal wear',
      description:'it includes kurtas, jeans, tops',
      GroupId:2,
      isactive:'true',
      createdAt: new Date(),
      updatedAt: new Date(),
      },{
      name: 'women ethnic wear',
      description:'it includes Saris, kurtas, lehenga sets palazzos, Anarkali kurtas etc,',
      GroupId:2,
      isactive:'false',
      createdAt: new Date(),
      updatedAt: new Date(),
      },{  
      name: 'kids party wear',
      description:'it includes suits, frocks etc',
      GroupId:3,
      isactive:'true',
      createdAt: new Date(),
      updatedAt: new Date(),
      }], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Categories', null, {});
  }
};
