'use strict';

const { INTEGER } = require("sequelize");

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('Products', [{
      name: 'plane white shirt',
      description: 'A classic solid shirt in white Can be worn for from office to after meeting evening get together.',
      image_url: 'https://cdn.shopify.com/s/files/1/0752/6435/products/MYRIAPINK--2_3ed24cd0-662f-4507-bb77-35c5af2a4dc7_900x.jpg?v=1654793590',
      CategoryId: 1,
      isactive: 'true',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'sherwani',
      description: 'The unconventional patterns on a light colored base, topped off with a glitzy neckline is a perfect choice for men, who like to keep it classy.',
      image_url: 'https://static01.manyavar.com/uploads/dealimages/13447/zoomimages/grandiose-fawn-groom-sherwani-odes787-304-7.jpg',
      CategoryId: 2,
      isactive: 'true',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'kurtas',
      description: 'Fabric : Cotton, Kurti Fabric : Cotton Blend, Kurta Color : Off White, Style: Striaght style Printed kurti Pant & Dupatta Set',
      image_url: 'https://rukminim1.flixcart.com/image/832/832/l0fm07k0/kurta/c/x/b/l-rama-strip-patta-tp-studio-original-imagc832g9p2yzuk.jpeg?q=70',
      CategoryId: 3,
      isactive: 'true',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'sarees',
      description: 'Style it with traditional jhumkas, matching neckpiece, wristwatch and embellished heels.',
      image_url: 'https://static.heerfashion.com/public/products/04-2022/s8qNdAhNF8qOAX64AFzxUT5lTyoiW8155NABlcn4.jpg',
      CategoryId: 4,
      isactive: 'true',
      createdAt: new Date(),
      updatedAt: new Date(),
    }, {
      name: 'suits',
      description: 'Brand - Babyhug, Fabric - Polyester/ Woven ,Sleeves - Full,Neck - Collar',
      image_url: 'https://cdn.fcglcdn.com/brainbees/images/products/438x531/9650999a.webp',
      CategoryId: 5,
      isactive: 'true',
      createdAt: new Date(),
      updatedAt: new Date(),
    }], {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('Products', null, {});
     */
    await queryInterface.bulkDelete('Products', null, {});
  }
};
