var express = require('express');
var router = express.Router();
var groupscontroller = require('../controller/groupscontroller');


router.get('/', groupscontroller.getAll);
router.get('/categories',groupscontroller.getAll)
router.get('/products/id',groupscontroller.getAll)
router.get('/withcategories',groupscontroller.getAllWithCategories);
router.get('/withproducts',groupscontroller.getAllWithProducts);


module.exports = router;