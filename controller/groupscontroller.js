var Model = require('../models');
const Categories = require('../models/categories.js');
module.exports.getAll = function (req, res) {
  Model.Groups.findAll()
    .then(function (groups) {
      res.send(groups);
    })
}

//only categories
var Model = require('../models');
const Category = require('../models/categories.js');
module.exports.getAll = function (req, res) {
  Model.Categories.findAll()
    .then(function (categories) {
      res.send(categories);
    })
}

//only products
var Model = require('../models');
module.exports.getAll = function (req, res) {
  Model.Products.findAll()
    .then(function (products) {
      res.send(products);
    })
}


//with categories
module.exports.getAllWithCategories = function (req, res) {
  Model.Groups.findAll({ include: Model.Categories })
    .then(function (groups) {
      res.send(groups);
    })
}


//with products
module.exports.getAllWithProducts = function (req, res) {
  Model.Groups.findAll({
    include: {
      model: Model.Categories,
      include: {
        model: Model.Products
      },
    }})
    .then(function (groups) {
      res.send(groups);
    })
}


//combine all
// var Model = require('../models');
// const router = require('../routes/demo');
// module.exports.getAll = (req, res) => {
//     Model.Groups.findAll({
//         include: {
//             model: Model.Categories,
//             include: {
//                model: Model.Products
//             },
//             // where: { isactive: 'true' },
//             // attributes: ['name'],
//         },
//     })
//         .then((user) => {
//             res.send(user);
//         })
//         .catch(err => {
//             res.status(500).send({
//                 message:
//                     err.message || "Some error occurred while retrieving tutorials."
//             });
//         });
// };
